import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {ScrollView, FlatList, TextInput, StyleSheet, TouchableOpacity, Text, View ,Image} from 'react-native';
import Apploading from "expo-app-loading";
import * as Font from "expo-font";
import colors from "./assets/colors/colors";
import { Feather } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import categoriesData from "./assets/data/categoriesData";
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import ListCategories from "./categoryList";
import Home from "./home";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Details from "./details";


const customFonts = {
  montserrat : require("./assets/fonts/Montserrat-Regular.ttf"),
  montserratBold : require("./assets/fonts/Montserrat-Bold.ttf"),
  montserratSemiBold : require("./assets/fonts/Montserrat-SemiBold.ttf"),
  montserratMedium : require("./assets/fonts/Montserrat-Medium.ttf"),
}

const Stack = createStackNavigator()

function Stacks() {

  return (
    <NavigationContainer initialRouteName = "home" >
      <Stack.Navigator  >
          <Stack.Screen
            name= "home"
            component = {Home}
            options = {{headerShown : false}}/>
          <Stack.Screen
            name = "details"
            component = {Details}
            options = {{headerShown : false}}/>
      </Stack.Navigator>

    </NavigationContainer>
  )

}

export default class App extends React.Component {
  state = {
    fontsLoaded : false
  }

  loadfonts = async() => {
    await Font.loadAsync(customFonts)
    this.setState({fontsLoaded : true})
  }

  componentDidMount(){
    this.loadfonts()
    console.log("component mounting")
  }

  componentWillUnmount(){
    this.setState({fontsLoaded: false})
  }
  render(){
    //console.log(colors.secondary)
    if(this.state.fontsLoaded)
    {
      return(
          <Stacks/>
      )
    }
    else
    {
      return (
          <Apploading/>
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: "#fff",
    //alignItems: 'center',
    //justifyContent: 'center',
  },


});
