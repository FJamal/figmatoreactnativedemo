import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {ScrollView, FlatList, TextInput, StyleSheet, TouchableOpacity, Text, View ,Image} from 'react-native';
import colors from "./assets/colors/colors";
import { Feather } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import categoriesData from "./assets/data/categoriesData";
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import ListCategories from "./categoryList";
import {useNavigation} from "@react-navigation/native";
import popularData from "./assets/data/popularData";



const Header = ()=> (
  <View style = {styles.header}>

    <Image
      style = {styles.profile}
      source = {require("./assets/images/profile.png")}/>

    <Feather name="menu" size={24} color={colors.darkText} />

  </View>
)

const Heading = ()=> (
  <View style = {styles.headingContainer}>
    <Text style = {{fontFamily : "montserrat", fontSize : 16, marginBottom : "0.60%"}}>Food</Text>
    <Text style = {{fontFamily : "montserratBold", fontSize : 32}}>Delivery</Text>
  </View>
)

const SearchBar = () => {
  const [text, onChangeText] = React.useState("")
  return (
  <View style = {styles.searchBarContainer}>
    <Feather name="search" size={16} color="black" />
    <TextInput
      style = {styles.search}
      onChangeText = {onChangeText}
      value = {text}
      placeholder = "search"
      placeholderTextColor = {colors.darkText}/>
  </View>
)}

const Categories = () => (
    <View style = {styles.categoriesContainer}>
      <Text style = {{fontFamily : "montserratBold", fontSize : 16}}>Categories</Text>
    </View>
)

// const CategoriesList = ()=> (
//   <View style = {styles.categoriesListContainer}>
//     <FlatList
//       contentContainerStyle={{ paddingRight :120, /*backgroundColor : "blue",*/  }}
//       data = {categoriesData}
//       renderItem = {(object) => <RenderCategories data = {object}/>}
//       //keyExtrator = {(item,index) => index.toString()}
//       horizontal = {true}/>
//   </View>
//
// )
//
//
// const RenderCategories = (obj) => {
//   //console.log(obj.data.item)
//
//   const[selected, setSelected] = useState(obj.data.item.selected)
//
//   const[selectedId, setSelectedId] = useState(1)
//
//
//
//    const onPress = (id)=> {
//      console.log("selected before click: ", selected)
//      console.log("id :",  id)
//      setSelected(previous=> !previous)
//      console.log("selected after click: ", selected)
//      setSelectedId(id)
//
//
//    }
//
//   useEffect(()=> {
//     console.log()
//    }, [selected, selectedId]
//  )
//   return (
//     <TouchableOpacity onPress = {()=>onPress(obj.data.item.id)}>
//       <View style = {[styles.listItems, {backgroundColor :  (selected && (selectedId == obj.data.item.id)) ? colors.primary : "#fff"}]} key = {obj.data.item.id}>
//         <Image resizeMode = "stretch" source = {obj.data.item.image} style = {styles.listItemImage}/>
//         <Text style = {styles.listItemTitle}>{obj.data.item.title}</Text>
//         <View
//           style = {[styles.iconwrapper, {backgroundColor : selected ? "#fff" : colors.secondary}]}>
//           <Feather name = "chevron-right" size = {8}
//             style = {[styles.categoryiconselect, {backgroundColor : selected ? "#fff" : colors.secondary}]}/>
//         </View>
//       </View>
//     </TouchableOpacity>
//
//   )
// }

const PopularList = () =>{
  const navigation = useNavigation()
  const onPress = (data) => navigation.navigate("details", {
    item : data
  })
  return (
    <View style = {styles.popularwrapper}>
      <Text style = {styles.popularTitle}>Popular</Text>
      {
        popularData.map((item) =>(
          <TouchableOpacity
            key = {item.id}
            onPress = {()=>onPress(item)}>
            <View style = {styles.popular} >
              <View  style = {styles.popularWrapper}>
                <View>
                  <View style = {styles.popularTopWrapper}>
                    <MaterialCommunityIcons name="crown" size={12} color={colors.primary} />
                    <Text style = {styles.popularoftheweek}>Top of the week</Text>
                  </View>
                  <View style = {styles.popularTitlesWrapper}>
                    <Text style = {styles.popularTitle}>{item.title}</Text>
                    <Text style = {styles.popularWeight}>Weight : {item.weight}</Text>
                  </View>
                  <View style = {styles.popularLastSection}>
                    <View style = {styles.popularplus}>
                      <AntDesign name="plus" size={10} color="black" />
                    </View>
                    <View style = {styles.ratingWrapper}>
                      <Ionicons name="star" size={10} color="black" />
                      <Text style = {{fontFamily : "montserratSemiBold", fontSize : 12}}>{item.rating}</Text>
                    </View>

                  </View>

                </View>
                <View style = {styles.popularImageWrapper}>
                  <Image resizeMode = "contain" source = {item.image}/>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        ))
      }
    </View>
)
}




export default class Home extends React.Component {

  componentDidMount(){

    console.log("Home component mounting")
  }

  render(){
      return (
        <View style = {styles.container}>
          <ScrollView contentContainerStyle= {{paddingBottom : 20}} nestedScrollEnabled = {true}>
            <Header/>
            <Heading/>
            <SearchBar/>
            <Categories/>
            <ListCategories/>
            <PopularList/>
          </ScrollView>
          <StatusBar style="dark" />
        </View>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  profile : {
    width : 40,
    height : 40,
    //resizeMode: "stretch",
    borderRadius : 40,
  },
  header : {
    marginTop : "8%",
    flexDirection : "row",
    //backgroundColor : "red",
    width : "100%",
    justifyContent : "space-between",
    alignItems : "center",
    paddingHorizontal : 20,
    //height : "100%"
  },
  headingContainer : {
    paddingLeft : "4.8%",
    marginTop : "4.6%",
  },
  search : {
    borderBottomWidth : 2,
    borderColor : "#CDCDCD",
    width : "75%",
    paddingLeft : "2.17%",
  },
  searchBarContainer : {
    flexDirection : "row",
    alignItems : "center",
    paddingLeft : "4.83%",
  },
  categoriesContainer : {
    marginTop : "3.80%",
    paddingLeft : "4.83%",
    //flex : 1
  },
  categoriesListContainer : {
    paddingTop : "1.84%",
    paddingBottom : "1.84%",
    width : "100%",
    paddingLeft : "4.83%",
    //height : "21.74%",

  //  flex : 1

  },
 listItems : {
    //flex : 1,
    //width : 100,
    backgroundColor : colors.primary,
    marginRight : 10,
    paddingRight : "2.42%",
    paddingLeft : "2.42%",
    borderRadius : 20,
    alignItems : "center",

    //width : "25.36%",
    //width : "",
    //height : "21.74%",

},
listItemImage : {
  // width : "63%",
  // height : "63%",
  //marginTop : "2.94%",
  marginTop : 20,
  //marginHorizontal : "4.83%",
  alignSelf : "center",
  width : 60,
  height :60,
},
listItemTitle : {
  //fontFamily : "montserratSemiBold",
  fontSize : 14,
  paddingTop : 10,
  textAlign : "center",
},
iconwrapper : {
  alignSelf : "center",
  height : 26,
  justifyContent : "center",
  marginTop : 20,
  marginBottom : 20,
  width : 26,
  borderRadius : 26,
  //backgroundColor : "red"
},
categoryiconselect : {
  alignSelf : "center",
},

/*Popular Section*/
popularTitle : {
  fontFamily : "montserratBold",
  fontSize : 16 ,
  marginBottom : 31,
},

popularwrapper : {
  paddingTop : 20,
  paddingLeft : 20,
  borderRadius : 24,


},
popularTopWrapper : {
  flexDirection : "row",
  paddingLeft : 10,
  alignItems : "center",
},
popularoftheweek : {
  fontFamily : "montserratSemiBold",
  fontSize : 14,
  paddingLeft : 10,
},
popular : {
  flexDirection : "row",
  paddingTop : 20,
},
popularTitlesWrapper : {
  marginTop : 20,
},
popularplus : {
  backgroundColor : colors.primary,
  alignItems : "center",
  width : 90,
  height : 53,
  justifyContent : "center",
  borderTopRightRadius : 25,
  borderBottomLeftRadius : 25,
},
popularLastSection : {
  paddingTop : 10,
  flexDirection : "row",
  alignItems: "center",

},
ratingWrapper : {
  flexDirection : "row",
  paddingLeft : 20,
  alignItems : "center",
},
popularTitle : {
  fontFamily : "montserratSemiBold",
  fontSize : 14
},
popularWeight : {
  fontFamily : "montserratMedium",
  fontSize : 12,
},
popularWrapper : {
  flexDirection : "row",
},
popularImageWrapper : {
  paddingLeft : 20,
}

});
