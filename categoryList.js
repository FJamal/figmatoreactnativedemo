import React from "react";
import {View, Text, Image, StyleSheet, FlatList, TouchableOpacity} from "react-native";
import categoriesData from "./assets/data/categoriesData";
import colors from "./assets/colors/colors";
import { Feather } from '@expo/vector-icons';

const CategoriesList = (props)=> (
  <View style = {styles.categoriesListContainer}>
    <FlatList
      contentContainerStyle={{ paddingRight :120, /*backgroundColor : "blue",*/  }}
      data = {categoriesData}
      renderItem = {(object) => renderCategories(object, props) }
      //keyExtrator = {(item,index) => index.toString()}
      horizontal = {true}
      extraData = {props.extraData}/>
  </View>

)


const renderCategories = (obj, props) => {
  //console.log(obj.data.item)

 //  const[selected, setSelected] = useState(obj.data.item.selected)
 //
 //  const[selectedId, setSelectedId] = useState(1)
 //
 //
 //
 //   const onPress = (id)=> {
 //     console.log("selected before click: ", selected)
 //     console.log("id :",  id)
 //     setSelected(previous=> !previous)
 //     console.log("selected after click: ", selected)
 //     setSelectedId(id)
 //
 //
 //   }
 //
 //  useEffect(()=> {
 //    console.log()
 //   }, [selected, selectedId]
 // )
 //console.log("extraData is", props.extraData)
  return (
    <TouchableOpacity onPress = {()=>props.onPress(obj.item.id)}>
      <View style = {[styles.listItems, {backgroundColor : (props.extraData === obj.item.id) ? colors.primary : "#fff"}]} key = {obj.item.id}>
        <Image resizeMode = "stretch" source = {obj.item.image} style = {styles.listItemImage}/>
        <Text style = {styles.listItemTitle}>{obj.item.title}</Text>
        <View
          style = {[styles.iconwrapper, {backgroundColor : (props.extraData === obj.item.id) ? "#fff" : colors.secondary}]}>
          <Feather name = "chevron-right" size = {8}
            style = {[styles.categoryiconselect, {backgroundColor : (props.extraData === obj.item.id) ? "#fff" : colors.secondary}]}/>
        </View>
      </View>
    </TouchableOpacity>

  )
}



export default class CategoryList extends React.Component {
  state = {
    SelectedItemId : null,
  }

  handleListPress = (id)=> {
    //console.log("The Clicked id is:", id)
    this.setState({SelectedItemId : id})
  }

  render(){
    return(
      <View style = {styles.container}>
        <CategoriesList
          onPress = {this.handleListPress}
          extraData = {this.state.SelectedItemId}/>
      </View>
    )
  }

}


const styles = StyleSheet.create({
  container : {
    backgroundColor : "#fff",
    flex : 1,
  },
  categoriesContainer : {
    marginTop : "3.80%",
    paddingLeft : "4.83%",
    //flex : 1
  },
  categoriesListContainer : {
    paddingTop : "1.84%",
    paddingBottom : "1.84%",
    width : "100%",
    paddingLeft : "4.83%",
    //height : "21.74%",

  //  flex : 1

  },
 listItems : {
    //flex : 1,
    //width : 100,
    backgroundColor : colors.primary,
    marginRight : 10,
    paddingRight : "2.42%",
    paddingLeft : "2.42%",
    borderRadius : 20,
    alignItems : "center",

    // width : 105,
    // //width : "",
    // height : 107,

},
listItemImage : {
  // width : "63%",
  // height : "63%",
  //marginTop : "2.94%",
  marginTop : 20,
  //marginHorizontal : "4.83%",
  alignSelf : "center",
  width : 60,
  height :60,
},
listItemTitle : {
  //fontFamily : "montserratSemiBold",
  fontSize : 14,
  paddingTop : 10,
  textAlign : "center",
},
iconwrapper : {
  alignSelf : "center",
  height : 26,
  justifyContent : "center",
  marginTop : 20,
  marginBottom : 20,
  width : 26,
  borderRadius : 26,
  //backgroundColor : "red"
},
categoryiconselect : {
  alignSelf : "center",
},
}

)
