import React from "react";
import {ScrollView, FlatList, View,TouchableOpacity, Text, StyleSheet, Image} from "react-native";
import { Feather, Ionicons } from '@expo/vector-icons';
import colors from "./assets/colors/colors";
import {useNavigation, useRoute} from "@react-navigation/native";


const Header = ()=> {
  const navigation = useNavigation()
  const onPress = ()=> {
    navigation.goBack()
  }
  return (
    <View style = {styles.headerWrapper}>
      <TouchableOpacity onPress = {onPress}>
        <View style = {styles.headerLeft}>
          <Feather name = "chevron-left" size = {8}/>
        </View>
      </TouchableOpacity>

      <View style = {styles.headerRight}>
        <Ionicons name = "star" size = {12}/>
      </View>

    </View>
)}


const Title = ()=> {
  const route = useRoute()
  return (
    <View style = {styles.titleWrapper}>
      <Text style = {styles.title}>{route.params.item.title}</Text>
      <View style = {styles.priceWrapper}>
        <Text style = {styles.price}>$ {route.params.item.price}</Text>
      </View>
    </View>
)
}

const PizzaDetails = ()=> {
  const route = useRoute()
  const item = route.params.item
  return (
    <View style = {styles.detailsWrapper}>
      <View style = {styles.infoWrapper}>
        <View style = {{marginTop : 20}}>
          <Text style = {styles.infoSmall}>Size</Text>
          <Text style = {styles.infoLarge}>{item.sizeName + " " + item.sizeNumber + "\""}</Text>
        </View>
        <View style = {{marginTop : 20}}>
          <Text style = {styles.infoSmall}>Crust</Text>
          <Text style = {styles.infoLarge}>{item.crust}</Text>
        </View>
        <View style = {{marginTop : 20}}>
          <Text style = {styles.infoSmall}>Delivery</Text>
          <Text style = {styles.infoLarge}>{item.deliveryTime + " min"}</Text>
        </View>
      </View>
      <View style ={styles.imageWrapper}>
        <Image style = {{width : 296, height : 196}}
          source = {item.image}
          resizeMode = "contain"/>
      </View>

    </View>
  )
}


const Ingredients = ()=> {
  const route = useRoute()
  const item = route.params.item
  console.log("item is ", item)
  return (
    <View style = {styles.ingredientsWrapper}>

      <Text style = {{fontFamily : "montserratBold", fontSize : 16}}>Ingredients</Text>
      <View style = {styles.ingredientsListWrapper}>
        <FlatList
          data = {item.ingredients}
          renderItem = {renderIngredients}
          horizontal = {true}
          showsHorizontalScrollIndicator = {false}/>
      </View>
    </View>
  )
}


const renderIngredients = ({item})=>{
  return (
    <View style = {{backgroundColor : "navajowhite", width : 100, height : 80, borderRadius : 15, marginRight : 15}}>
      <Image
        style = {{width : 100, height : 80,}}
        resizeMode = "center"
        source = {item.image}/>
    </View>
  )
}


const Button = ()=> (
  <TouchableOpacity
    style = {{alignSelf : "center"}}
    onPress = {()=> alert("Your order was placed... Well not Really, this is a demo app, for placing the order I have to code the backend as well")}>
    <View style = {styles.button}>
      <Text style = {{fontFamily : "montserratBold", fontSize : 14,}}>Place an Order</Text>
      <Feather name = "chevron-right" size = {12} style = {{marginLeft : 10}}/>
    </View>
  </TouchableOpacity>
)


export default class Details extends React.Component {

  componentDidMount() {

    //console.log("data been send is", this.props.route.params)
  }

  render(){
    return (
      <View style = {styles.container}>
        <ScrollView>
          <Header/>
          <Title/>
          <PizzaDetails/>
          <Ingredients/>
          <Button/>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    //backgroundColor : "lightslategrey"
    backgroundColor : colors.background,
    // justifyContent : "center",
  },
  headerRight: {
    width: 40,
    height : 40,
    backgroundColor : colors.primary,
    alignItems : "center",
    justifyContent : "center",
    marginRight : 20,
    borderRadius: 8,
  },
  headerLeft : {
    width : 40,
    height : 40,
    alignItems : "center",
    justifyContent : "center",
    marginLeft : 20,
    borderRadius : 8,
    borderWidth : 2,
    borderColor : colors.lightText,
    borderRadius: 8,
  },
  headerWrapper : {
    width : "100%",
    justifyContent : "space-between",
    flexDirection : "row",
    marginTop : 60,
    alignItems : "center"
  },
  titleWrapper : {
    width : 182,
    height : 78,
    marginTop : 30,
    marginLeft : 20,
  },
  title : {
    fontSize : 32,
    fontFamily : "montserratBold",
  },

  priceWrapper : {
    marginTop : 20,

  },
  price : {
    fontFamily : "montserratSemiBold",
    fontSize : 32,
    color : colors.price,
  },
  infoSmall : {
    fontFamily : "montserrat",
    color: colors.darkText,
    fontSize : 14
  },
  infoLarge : {
    fontFamily : "montserratSemiBold",
    fontSize : 16,
    paddingTop : 5,
  },
  detailsWrapper : {
    marginTop : 60,
    marginLeft : 20,
    flexDirection : "row",
    //alignItems : "center"
  },
  imageWrapper : {
    marginLeft : 37,
    overflow : "hidden",
    alignSelf : "center",
  },
  ingredientsWrapper : {
    marginTop : 60,
    marginLeft : 20,
  },
  ingredientsListWrapper : {
    marginTop : 19,
  },
  button : {
    width : 335,
    height : 65,
    backgroundColor : colors.primary,
    borderRadius : 50,
    alignItems : "center",
    justifyContent : "center",
    marginTop : 40,
    flexDirection : "row",
    paddingTop : 23,
    paddingBottom : 22,
  }
})
