const CategoriesData = [
  {
    id : "1",
    image : require("../images/pizza-icon.png"),
    title : "Pizza",
    selected : true,
  },
  {
    id : "2",
    image : require("../images/shrimp-icon.png"),
    title : "Sea Food",
    selected : false,
  },
  {
    id : "3",
    image : require("../images/soda-icon.png"),
    title : "Drinks",
    selected : false,
  },
  {
    id : "4",
    image : require("../images/pizza-icon.png"),
    title : "Pizza",
    selected : false,
  },
  {
    id : "5",
    image : require("../images/shrimp-icon.png"),
    title : "Sea Food",
    selected : false,
  },
  {
    id : "6",
    image : require("../images/soda-icon.png"),
    title : "Drinks",
    selected : false,
  },
]

export default CategoriesData;
