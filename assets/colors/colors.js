const colors = {
  background : "#F9F9FB",
  darkText : "#313234",
  lightText : "#CDCDCD",
  primary : "#F5CA48",
  secondary : "#F26C68",
  price : "#E4723C",
}

export default colors;
